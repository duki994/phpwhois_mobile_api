<?php


$query= trim(@$_POST['query']);
//delete www. from query if exists
$query = preg_replace('/www./', '', $query);

header('Content-Type: text/html; charset=UTF-8');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
    <script type="text/javascript">
          function SaveQueryAndroid() {
            var query = document.getElementById("whois-query").value;
            //send to Android JS WebView interface
            window.WhoIsJS.GetQuery(query);
          }

          function SavehtmlData() {
            var htmlData = document.getElementById("whois-data").innerHTML;
            //send to Android JS WebView interface
            window.WhoIsJS.GethtmlData(htmlData);

          }
    </script>
    <style type="text/css">
        html, body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            display: table;
        }
        .container {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }
    </style>
</head>
<body onload="SavehtmlData();">

<div class="container">

<!-- WhoIs form - START -->
<form class="col-md-12" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <div class="form-group">
        <input type="text" id="whois-query" onblur="SaveQueryAndroid()" name="query" class="form-control input-lg" placeholder="Enter website domain/URL">
    </div>
    <div class="form-group">
        <input type="submit" id="whois-send" class="btn btn-primary btn-lg btn-block" value="WhoIs Query"/>
    </div>
</form>
<!-- WhoIs Form - END -->

    <div class="panel panel-default">
    <div class="panel-heading">WhoIs data</div>
        <div id="whois-data" class="panel-body">
            <?php
               include_once('phpwhois/whois.main.php');
               include_once('phpwhoisutils/whois.utils.php');

               $whois= new Whois();
               $result= $whois->Lookup($query);

               if (!empty($result['rawdata']))
               {
                  $utils= new utils;
                  $echoHTML = $utils->showHTML($result);
                  if (!empty($echoHTML)) {
                    echo $echoHTML;
                    echo "<br />\n";
                  } else {
                    $html = "<b>No results for entered domain or it doesn't exist.</b>";
                    echo $html;
                  }
               }
               else
               {
                  echo implode($whois->Query['errstr'],"<br />\n");
               }
            ?>
        </div>
    </div>

</div>
</body>
</html>