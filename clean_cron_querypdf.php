<?php

/* delete all files older than 24h */
$path = 'queryPDF/';
if ($handle = opendir($path)) {

    while (false !== ($file = readdir($handle))) {

        if ((time()-filectime($path.$file)) >= 86400) {  
            unlink($path.$file);
        }

    }
}

?>